import React, {Component } from "react"
import Main from "./Main"
import Landing from "./Components/Landing/Landing"

class App extends Component{
    constructor(props){
        super()
        this.state = {
            Landing: true
        }
    }
    handleStartCallButton = () => {
        this.setState({Landing: false})
    }
    render(){
        return(
            <>
            {this.state.Landing? <Landing handleStartCallButton={this.handleStartCallButton}/> :  <Main />}
           </>
        )
    }

}

export default App