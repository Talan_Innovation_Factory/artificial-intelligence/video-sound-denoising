export const doLogin = async (username, database, handleUpdate) => {
    await database.ref('/connectedUsers/' + username).set({
      status: "connected"
    })
  
    await database.ref('/notifs/' + username).remove()
    database.ref('/notifs/' + username).on('value', snapshot => {
      snapshot.exists() && handleUpdate(snapshot.val())
    })
  }
  
  // Write an offer to the database
  export const doOffer = async (to, offer, database, username) => {
    await database.ref('/notifs/' + to).set({
      type: 'offer',
      from: username,
      offer: JSON.stringify(offer)
    })
  }
  
  // Write an answer to the database
  export const doAnswer = async (to, answer, database, username) => {
    await database.ref('/notifs/' + to).update({
      type: 'answer',
      from: username,
      answer: JSON.stringify(answer)
    })
  }
  
  // Write a leave notification when user disconnects
  export const doLeaveNotif = async (to, database, username) => {
    await database.ref('/notifs/' + to).update({
      type: 'leave',
      from: username
    })
  }
  
  export const doCandidate = async (to, candidate, database, username) => {
    // send the new candiate to the peer
    await database.ref('/notifs/' + to).update({
      type: 'candidate',
      from: username,
      candidate: JSON.stringify(candidate)
    })
  }

  // Submit user rating to the database
  export const submitRating = async (database, username, rating, comment) => {
    await database.ref('/Rating/' + username + ' on '+ new Date().toString().substring(0,24) ).set({
      rating: rating,
      comment: comment,
    })
  }

  // Add connected username to the database
  export const addConnectedUser = async (database, username) => {
    await database.ref('/connectedUsers/').set({
      user: username
    })
  }

  // Remove connected username from the database
  export const removeConnectedUser = async (database, username) => {
    await database.ref('/connectedUsers/' + username).remove()
  }

  // Check if username already exists in the database
  export const usernameExist = async (database, username) => {
    let exist = false
    await database.ref('/connectedUsers/'+ username).once("value", snapshot =>{
      if(snapshot.exists()){
        exist = true
      }
    })
    return exist
  }
