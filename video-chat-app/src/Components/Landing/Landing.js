import React, {Component } from "react"
import './Landing.css'
import Logo from './talan_meet.PNG'
import Button from '@material-ui/core/Button';

export default class VideoContainer extends Component{
    render(){
        return(
            <div className='container'>
                <img src={Logo}/>
                <div style={{marginLeft:"45%"}}>
                    <Button variant="contained" color="primary" size="large" onClick={this.props.handleStartCallButton}>
                        START CALL
                    </Button>
                </div>
                <p className='par'>Talan meet is free peer to peer video conferencing service you can use to virtually 
                meet with others. It was developed during the COVID-19 pandemic to enable people to safely 
                connect with family and friends.
                </p>
                <p className='par'>Help fight COVID-19. <a href="https://www.google.com/covid-19" >Learn more</a></p> 
            </div>
        )
    }
}