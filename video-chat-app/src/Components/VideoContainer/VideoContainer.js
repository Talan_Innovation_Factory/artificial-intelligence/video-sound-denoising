import React, {Component } from "react"
import './VideoContainer.css'
import Subtitles from '../SpeechRecognition/SpeechRecognition'
import Loader from "react-loader-spinner";
import { faMicrophone } from '@fortawesome/free-solid-svg-icons'
import { faMicrophoneSlash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faVideo } from '@fortawesome/free-solid-svg-icons'
import { faVideoSlash } from '@fortawesome/free-solid-svg-icons'
export default class VideoContainer extends Component{

    constructor(props) {
        super(props);

        this.state = {
            diffX: 0,
            diffY: 0,
            dragging: false,
            styles: {}
        }
        this._dragStart = this._dragStart.bind(this);
        this._dragging = this._dragging.bind(this);
        this._dragEnd = this._dragEnd.bind(this);
    }

    /* Functions to make the local stream element draggable */
    _dragStart(e) {
        this.setState({
            diffX: e.screenX - e.currentTarget.getBoundingClientRect().left,
            diffY: e.screenY - e.currentTarget.getBoundingClientRect().top,
            dragging: true
        });
    }

    _dragging(e) {
        if(this.state.dragging) {
            var left = e.screenX - this.state.diffX;
            var top = e.screenY - this.state.diffY;

            this.setState({
                styles: {
                    left: left,
                    top: top
                }
            });
        }
    }

    _dragEnd() {
        this.setState({
            dragging: false
        });
    }

    render(){
        return(
            <>
            <div 
            className="moveable" 
            style={this.state.styles} onMouseDown={this._dragStart} onMouseMove={this._dragging} onMouseUp={this._dragEnd}>
                {!this.props.cam?<p className="info">Video disabled</p>:<></>}
                <video
                className="local-video"
                ref={this.props.localVideoRef}
                autoPlay playsInline muted="muted"></video>
                


           </div>
                {!this.props.connectedUser?<div>
                    <Loader type="Puff" color="#a9a9ad" height={200} width={200} timeout={0} className="loader"/>
                    <p className="waiting">Waiting for other participant to join the call ...</p>
                    
                    
                </div>
                :<></>}
                <Subtitles
                subtitles={this.props.subtitles}
                remoteTranslation={this.props.remoteTranslation}
                connectedUser={this.props.connectedUser}
                mic={this.props.mic}
                sendMessage={this.props.sendMessage}
                className="video_overlays"
                />
                <div className="xD">
                    {this.props.connectedUser?<p className="inputs">
                            {this.props.remoteMicStatus?<FontAwesomeIcon icon={faMicrophone} />:<FontAwesomeIcon icon={faMicrophoneSlash} />}
                            {this.props.remoteCamStatus?<FontAwesomeIcon icon={faVideo} />:<FontAwesomeIcon icon={faVideoSlash} />}
                    </p>:<></>}

                    <video
                        className={this.props.connectedUser?"remote-video":"remote-video-off"}
                        ref={this.props.setRemoteVideoRef}  autoPlay playsInline>
                    </video>
                </div>
           </>
        );
    }
}