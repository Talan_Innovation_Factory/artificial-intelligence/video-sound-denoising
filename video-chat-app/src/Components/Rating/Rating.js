import React, {Component } from "react"
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Rating from '@material-ui/lab/Rating';
import SentimentVeryDissatisfiedIcon from '@material-ui/icons/SentimentVeryDissatisfied';
import SentimentDissatisfiedIcon from '@material-ui/icons/SentimentDissatisfied';
import SentimentSatisfiedIcon from '@material-ui/icons/SentimentSatisfied';
import SentimentSatisfiedAltIcon from '@material-ui/icons/SentimentSatisfiedAltOutlined';
import SentimentVerySatisfiedIcon from '@material-ui/icons/SentimentVerySatisfied';
import TextField from '@material-ui/core/TextField';
import { submitRating } from '../../Modules/FirebaseModule'
import Typography from '@material-ui/core/Typography';
import SaveIcon from '@material-ui/icons/Save';
import StarIcon from '@material-ui/icons/Star';
import Box from '@material-ui/core/Box';


export default class RatingDiag extends Component {

    constructor(props){
        super(props)
        this.state = {
          open: this.props.displayRating,
          comment: '',
          rating: '',
          hover : ''
        }
      }

    handleClose = () => {
      this.props.setDisplayRating()
      this.props.clearTranscript()
      };

    handleRatingChange = (e,value) => {
      this.setState({rating: value})
    }

    handleComment = (e) =>{
      this.setState({comment: e.target.value})
    }

    handleSubmit = () =>{
      /* Save the rating and comment to the database */
      this.handleClose()
        submitRating(this.props.database, this.props.username  ,this.state.rating, this.state.comment)
    }

render() {

    function IconContainer(props) {
        const { value, ...other } = props;
        return <span {...other}>{customIcons[value].icon}</span>;
      }
      
      IconContainer.propTypes = {
        value: PropTypes.number.isRequired,
      };

      const labels = {
        0.5: 'Useless',
        1: 'Useless+',
        1.5: 'Poor',
        2: 'Poor+',
        2.5: 'Ok',
        3: 'Ok+',
        3.5: 'Good',
        4: 'Good+',
        4.5: 'Excellent',
        5: 'Excellent+',
      };

    const customIcons = {
        1: {
          icon: <SentimentVeryDissatisfiedIcon />,
          label: 'Very Dissatisfied',
        },
        2: {
          icon: <SentimentDissatisfiedIcon />,
          label: 'Dissatisfied',
        },
        3: {
          icon: <SentimentSatisfiedIcon />,
          label: 'Neutral',
        },
        4: {
          icon: <SentimentSatisfiedAltIcon />,
          label: 'Satisfied',
        },
        5: {
          icon: <SentimentVerySatisfiedIcon />,
          label: 'Very Satisfied',
        },
      };
     
    return(
        <Dialog
          disableBackdropClick
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          maxWidth='sm'
          fullWidth
        >
          <DialogTitle id="alert-dialog-title">{"We seek your feedback !"}</DialogTitle>

          <DialogContent  dividers>
            <Typography display="block">Conversation transcript is ready.</Typography>
            <Button
              variant="outlined"
              color="inherit"
              size="small"
              startIcon={<SaveIcon />}
              onClick={this.props.handleTranscriptDownload}
              style={{marginTop: '5px'}}
            >
              Save
            </Button>
          </DialogContent>
           
          <DialogContent dividers>
            
            <Typography>Add Comment (optional)</Typography>
            <TextField
              onChange={this.handleComment}
              autoFocus
              margin="dense"
              id="name"
              label="Comment"
              type="text"
              fullWidth       
            />
        </DialogContent>

        <DialogContent >
        <Typography>How was the audio and video?</Typography>
            <Rating
              name="hover-feedback"
              value={this.state.rating}
              precision={0.5}
              onChange={(event, newValue) => {
                this.setState({rating: newValue})
              }}
              onChangeActive={(event, newHover) => {
                this.setState({hover: newHover})
              }}
              emptyIcon={<StarIcon style={{ opacity: 0.55 }} fontSize="inherit" />}
            />
      {this.state.rating !== null && (
        <Box sx={{ ml: 2 }}>{labels[this.state.hover !== -1 ? this.state.hover : this.state.rating]}</Box>
      )}

        </DialogContent>
          
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
                Exit
            </Button>
            <Button onClick={this.handleSubmit} color="primary">
                Submit
            </Button>
            
          </DialogActions>
        </Dialog>
    )
}
}