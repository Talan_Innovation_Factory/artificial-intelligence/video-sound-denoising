import React, {Component } from "react"
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import TextField from '@material-ui/core/TextField';
import { usernameExist } from '../../Modules/FirebaseModule'
import CircularProgress from '@material-ui/core/CircularProgress';

export default class InviteBox extends Component {

    constructor(props){
        super(props)
        this.state = {
          open: true,
          username: null,
          error:false,
          userToCall: null,
          helperText: 'Name is required',
          loading: false
        }
      }

      handleClose = () => {
        this.setState({open: false})
        this.props.handleInvite()
    }

      handleChange = (e) =>{
        this.setState({userToCall: e.target.value}) 
    }

    handleJoin = () => {
        /* A function to handle the invitation of the other peer to the call */
        if(!this.state.userToCall)
        this.setState({error: true})
        else{
            this.setState({loading: true})
            usernameExist(this.props.database,this.state.userToCall).then((userExist) => {
                if(userExist && this.state.userToCall !== this.props.username){
                    /* If user exist, start the call and close dialog box */
                    this.props.startCall(this.state.userToCall)
                    this.handleClose()
                }
                else{
                    /* Display error message if invited user does not exist */
                    this.setState({loading: false, error: true , helperText: "User does not exist"})
                }
            }
        
            )}
        }

    render(){
        return (
            <div>

                <Dialog
                    disableBackdropClick
                    open={this.state.open}
                    onClose={this.handleClose}
                    aria-labelledby="alert-dialog-title"
                    aria-describedby="alert-dialog-description"
                >

                    <DialogTitle id="alert-dialog-title">{"START CALL"}</DialogTitle>
                    <DialogContent>
                        
                         <TextField required 
                            label="NAME" style={{marginBottom:'20px'}} 
                            onChange={this.handleChange}
                            helperText={this.state.helperText}
                            error={this.state.error}
                            />

                    </DialogContent>
                    <DialogActions>
                    <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        {this.state.loading?<CircularProgress size={30} thickness={6} />
                        :<Button onClick={this.handleJoin} color="primary">
                            Join
                        </Button>}
                    </DialogActions>
                    
                </Dialog>
            </div>
        );
    }
}