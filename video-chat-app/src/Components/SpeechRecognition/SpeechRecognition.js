import React, {useEffect } from "react"
import SpeechRecognition, { useSpeechRecognition } from 'react-speech-recognition'
import './SpeechRecognition.css'


const Subtitles = (props) => {

  const {connectedUser,mic, sendMessage} = props
  const { transcript, resetTranscript } = useSpeechRecognition()

   useEffect(() => {
     if(connectedUser && mic)
     {
       sendMessage(transcript)
     }
  }, [transcript])

  if (!SpeechRecognition.browserSupportsSpeechRecognition()) {
    return null
  }

  SpeechRecognition.startListening({ continuous: true, language: props.language })
  //!props.language?SpeechRecognition.startListening({ continuous: true }):SpeechRecognition.abortListening()
  

  return (
    <div>
      {props.subtitles?<div className="substyle">
          <p>{props.remoteTranslation.split(" ").splice(-10).join(" ")}</p>
      </div>:<></>}
    </div>
  )
}
export default Subtitles