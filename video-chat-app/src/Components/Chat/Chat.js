import React, {Component } from "react"
import './Chat.css'
import SentMessage from'./SentMessage'
import RecievedMessage from './ReceivedMessage'

export default class Chat extends Component {
    constructor(props){
        super(props)
        this.state = { message: ''}
    }
    
    handleInputChange = (e) => {
        this.setState({message: e.target.value})
    }

    handleMessageSubmit = (e) =>{
        e.preventDefault()
        this.props.appendOwnMessage(this.state.message)
        this.props.ChatChannelSendMessage(this.state.message)
        this.setState({message: ''})
    }

    scrollToBottom = () => {
        /* Keep the latest messages visible on the chat section */
        this.messagesEnd && this.messagesEnd.scrollIntoView({ behavior: "smooth" });
      }
      
      componentDidMount() {
        this.scrollToBottom();
      }
      
      componentDidUpdate() {
        this.scrollToBottom();
      }
      
    render(){ 
        return(
            <>
            <div>
                <div id="chat-zone">
                    <div class="chat-messages" >
                        {
                            this.props.messagesList.data.map((item)=>
                            item.type === 'received'?<div ref={(el) => { this.messagesEnd = el; }}><RecievedMessage msg={item.message}/></div>
                            :<div ref={(el) => { this.messagesEnd = el; }}><SentMessage msg={item.message} /></div>
                            )
                        }
                    </div>
                </div>
                <form class="compose" onSubmit={this.handleMessageSubmit}>
                    <input type="text" placeholder="Type a message" onChange={this.handleInputChange} value={this.state.message} />
                </form>   
            </div>
            </>
        );
    }
}