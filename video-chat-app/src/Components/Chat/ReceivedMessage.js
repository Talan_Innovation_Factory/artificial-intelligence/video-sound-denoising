import React, {Component } from "react"
import './Chat.css'


export default class ReceivedMessage extends Component{
    render(){
        return(
            <div class="message-item moderator cssanimation fadInBottom">
                <div class="message-bloc">
                    <div class="message">{this.props.msg}</div>
                </div>
            </div>
        )

    }
}