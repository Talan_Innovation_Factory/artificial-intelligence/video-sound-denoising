import React, {Component } from "react"
import './Chat.css'


export default class SentMessage extends Component{
    render(){
        return(
            <div class="message-item customer cssanimation fadInBottom">
                <div class="message-bloc">
                    <div class="message">{this.props.msg}</div>
                </div>
            </div>
        )

    }
}