import React, {Component } from "react"
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch';
import TextField from '@material-ui/core/TextField';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMicrophone } from '@fortawesome/free-solid-svg-icons'
import { faVideo } from '@fortawesome/free-solid-svg-icons'
import { usernameExist } from '../../Modules/FirebaseModule'
import Select from '@material-ui/core/Select';
import CircularProgress from '@material-ui/core/CircularProgress';



export default class DialogBox extends Component {

  constructor(props){
    super(props)
    this.state = {
      open: true,
      microphone: this.props.mic,
      camera: this.props.cam,
      username: null,
      error:false,
      helperText: "Name is required",
      micInputList: [],
      camInputList: [],
      micId: '',
      camId: '',
      loading: false
    }
  }

  componentDidMount(){
    this.getUserMedia() 
  }

  getConnectedDevices = (type, callback) => {
    /* Get all user media devices corresponding to the type */
    navigator.mediaDevices.enumerateDevices()
        .then(devices => {
            const filtered = devices.filter(device => device.kind === type);
            callback(filtered);
        });
  }

  handleClose = () => {
    /* Handle the closing of the dialog box */
      if(!this.state.username){
        this.setState({error: true})
      } 
      else{
        this.setState({loading: true})
        /* Denying duplicate usernames */
        usernameExist(this.props.database,this.state.username).then((userExist) => {
          if(!userExist){
            this.setState({open: false})
            this.props.updateUsername(this.state.username)
          }
          else{
            this.setState({loading:false, error: true , helperText: "Username not available"})
          }
        })
        
      }
      
    };

  handleChange = (event) => {
    /* Handle toggle mic/cam on and off */
        this.setState({
          [event.target.name]: event.target.checked,
        })
        if (event.target.name === 'microphone'){
          this.props.toggleMic()
        }
        if (event.target.name === 'camera'){
          this.props.toggleCam()
        }

      };

  updateUsername = (e) =>{
    this.setState({username: e.target.value})
    }

  onAudioChange = (event) =>{
    /* Handle selected mic id */
    this.setState({micId: event.target.value},()=>{this.getUserMedia()})
    this.props.updateMicId(event.target.value)
    }
      
  onVideoChange = (event) =>{
    /* Handle selected cam id */
    this.setState({camId: event.target.value},()=>{this.getUserMedia()})
    this.props.updateCamId(event.target.value)
      }

  getUserMedia = () => {
    /* Ask for permission to access user media devices */
    navigator.mediaDevices
        .getUserMedia({video: {deviceId: this.state.camId },
                      audio: {deviceId: this.state.micId, sampleSize: 16}
        })
        .then(stream =>{
          stream.getVideoTracks()[0].enabled = this.props.cam
          stream.getAudioTracks()[0].enabled = this.props.mic
          this.props.setLocalStreamAndVideoRef(stream)
          this.getConnectedDevices('audioinput', audioInput => {
            this.setState({micInputList: audioInput, micId: audioInput[0].deviceId})
          });
      
          this.getConnectedDevices('videoinput', videoInput => {
            this.setState({camInputList: videoInput, camId: videoInput[0].deviceId})
          });

          })
        .catch(console.log);
      }

      render(){
    return (
      <div>
        <Dialog
          disableBackdropClick
          open={this.state.open}
          onClose={this.handleClose}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description"
          maxWidth="md"
        >
          <DialogTitle id="alert-dialog-title">{"Join meeting"}</DialogTitle>

          <DialogContent dividers>
            <TextField required defaultValue={this.props.username}
            label="NAME" style={{marginBottom:'20px'}} onChange={this.updateUsername} 
            error={this.state.error}
            helperText={this.state.helperText}
            fullWidth
            />
          </DialogContent>
          
          <DialogContent >
              <FontAwesomeIcon icon={faMicrophone} style={{marginRight: '10px'}} />
                <Select
                native
                  value={this.state.micId}
                  onChange={this.onAudioChange}
                  inputProps={{
                    name: 'age',
                    id: 'age-native-simple',
                  }}
                  style={{minWidth: 500}}
                >
                  {this.state.micInputList.map((item) => 
                  // <MenuItem >{item.label}</MenuItem>
                  <option value={item.deviceId}>{item.label}</option>
                  )}
                </Select>
              <FormControlLabel 
              control={<Switch 
                          checked={this.state.microphone} 
                          onChange={this.handleChange} 
                          onClick={this.props.toggleMic} 
                          name="microphone" color="primary"/>} 
                  labelPlacement="bottom"/>
            </DialogContent>

            <DialogContent>
              <FontAwesomeIcon icon={faVideo} style={{marginRight: '10px'}} />
              <Select
                  style={{minWidth: 500}}
                  native
                  onChange={this.onVideoChange}
                  inputProps={{
                    name: 'age',
                    id: 'age-native-simple',
                  }}
                >
                  {this.state.camInputList.map((item) => 
                  <option value={item.deviceId}>{item.label}</option>
                  )}
                </Select>
                <FormControlLabel 
                    control={<Switch checked={this.state.camera} 
                                onChange={this.handleChange} 
                                name="camera" 
                                edge="start"
                                color="primary"/>} 
                    labelPlacement="top"/>
          </DialogContent>

          <DialogActions style={{margin: '15px'}}>
          {this.state.loading?<CircularProgress size={30} thickness={6} />:
            <Button onClick={this.handleClose} color="primary">
                Join now
            </Button>}
          </DialogActions>

        </Dialog>
      </div>
    );
  }
  }