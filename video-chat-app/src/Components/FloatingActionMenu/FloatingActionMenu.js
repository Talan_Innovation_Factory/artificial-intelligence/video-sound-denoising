import React, {Component } from "react"
import './FloatingActionMenu.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faMicrophone } from '@fortawesome/free-solid-svg-icons'
import { faMicrophoneSlash } from '@fortawesome/free-solid-svg-icons'
import { faVideo } from '@fortawesome/free-solid-svg-icons'
import { faVideoSlash } from '@fortawesome/free-solid-svg-icons'
import { faDesktop } from '@fortawesome/free-solid-svg-icons'
import { faCommentSlash } from '@fortawesome/free-solid-svg-icons'
import { faComment } from '@fortawesome/free-solid-svg-icons'
import { faUserFriends } from '@fortawesome/free-solid-svg-icons'
import { faClosedCaptioning } from '@fortawesome/free-solid-svg-icons'
import { faMicrophoneAlt } from '@fortawesome/free-solid-svg-icons'
import { faPhoneSlash } from '@fortawesome/free-solid-svg-icons'

export default class FloatingActionMenu extends Component{

    render(){
        
        return(
            <div className="multi-button">
                <div className="buttonContainer" onClick={this.props.toggleMic}>
                    <button className="hoverButton">
                    {this.props.mic?
                    <FontAwesomeIcon icon={faMicrophone} />
                    :<FontAwesomeIcon icon={faMicrophoneSlash} />
                    }
                    </button>
                    {this.props.mic?<div className="HoverState" id="mic-text">Mute</div>:
                    <div className="HoverState" id="mic-text">Unmute</div>
                    }
                </div>


                <div className="buttonContainer" onClick={this.props.toggleCam}>
                    <button className="hoverButton" >
                    {this.props.cam?
                    <FontAwesomeIcon icon={faVideo} />
                    :<FontAwesomeIcon icon={faVideoSlash} />
                    }
                    </button>
                    {this.props.cam?<div className="HoverState" id="video-text">Pause Video</div>:
                    <div className="HoverState" id="video-text">Unpause Video</div>
                    }
                </div>

                <div className="buttonContainer">
                    <button className="hoverButton" id="share-button" onClick={this.props.handleShareScreen}>
                    <FontAwesomeIcon icon={faDesktop} />
                    </button>
                    <div className="HoverState" id="swap-text" >Share Screen</div>
                </div>

                <div className="buttonContainer" onClick={this.props.toggleChat}>
                    <button className="hoverButton" >
                    {this.props.chat?<FontAwesomeIcon icon={faComment} />:
                    <FontAwesomeIcon icon={faCommentSlash} />
                    }
                    </button>
                    {!this.props.chat?<div className="HoverState" id="chat-text">Show Chat</div>:
                    <div className="HoverState" id="chat-text">Hide Chat</div>
                    }
                </div>

                <div className="buttonContainer" onClick={this.props.handleInvite}>
                    <button
                    className="hoverButton"
                    id="pip-button"
                    >
                    <FontAwesomeIcon icon={faUserFriends} />
                    </button>
                    <div className="HoverState" id="pip-text">Invite</div>
                </div>

                <div className="buttonContainer" onClick={this.props.toggleSubtitles}>
                    <button className="hoverButton">
                    <FontAwesomeIcon icon={faClosedCaptioning} />
                    </button>
                    <div className="HoverState" id="caption-button-text" >
                    Start Live Caption
                    </div>
                </div>

                <div className="buttonContainer">
                    <button className="hoverButton">
                    <FontAwesomeIcon icon={faMicrophoneAlt} />
                    </button>
                    <div className="HoverState" id="caption-button-text">
                    Start Speech Enhancement
                    </div>
                </div>

                <div className="buttonContainer" onClick={this.props.leaveCall}>
                    <button
                    className="hoverButton"
                    >
                    <FontAwesomeIcon icon={faPhoneSlash} />
                    </button>
                    <div className="HoverState">End Call</div>
                </div>
                
                </div>
                
        );
    }
}