import React, {Component } from "react"
import './App.css';
import DialogBox from './Components/DialogBox/DialogBox'
import VideoContainer from './Components/VideoContainer/VideoContainer'
import { createOffer, initiateConnection, startCall,sendAnswer, 
  addCandidate, listenToConnectionEvents, initiateDataChannel,} from './Modules/RTCModule'
import { doOffer, doAnswer, doLogin, doCandidate, doLeaveNotif, removeConnectedUser } from './Modules/FirebaseModule'
import config from './config'
import 'webrtc-adapter'
import firebase from 'firebase/app'
import 'firebase/database'
import FloatingActionMenu from './Components/FloatingActionMenu/FloatingActionMenu'
import InviteBox from './Components/InviteBox/InviteBox'
import RatingDiag from './Components/Rating/Rating'
import Chat from './Components/Chat/Chat'
import chatNotification from './Components/Sounds/juntos-607.mp3'
import endCallNotification from './Components/Sounds/case-closed-531.mp3'
import joinCallNotification from './Components/Sounds/communication-channel-519.mp3'
import talanLogo from'./images/talan_logo.jpg'
import MuiAlert from '@material-ui/lab/Alert';

class Main extends Component{
  constructor(props){

    (!firebase.apps.length)?firebase.initializeApp(config):firebase.app()

    super(props)

    // Create video elements references
    this.localVideoRef = React.createRef()
    this.remoteVideoRef = React.createRef()

    this.state = {
      mic: false,
      cam: false,
      chat: false,
      localStream: '',
      database: firebase.database(),
      localConnection: null,
      connectedUser: null,
      username: null,
      NoiseReduction: false,
      subtitles: false,
      SRChannel: null,
      chatSendChannel: null,
      mediaInputChannel: null,
      localTranslation: '',
      remoteTranslation: '',
      language: window.navigator.userLanguage || window.navigator.language,
      invite: false,
      screeShare: false,
      sender: null,
      displayRating: false,
      messagesList: {data:[]},
      micId : null,
      camId: null,
      remoteMicStatus: false,
      remoteCamStatus: false,
      captionInfo: false
    }
    
  }

componentDidMount(){
  /* Execute setLocalConnection when the application load */
  this.setLocalConnection()
}

playChatNotificationSound = () =>{
  /* Play notification sound when a chat message is recieved */
  const audio = new Audio(chatNotification)
  audio.load()
  audio.play()
}

playEndCallNotificationSound = () =>{
  /* Play notification sound when a call is ended */
  const audio = new Audio(endCallNotification)
  audio.load()
  audio.play()
}

playjoinCallNotificationSound = () =>{
  /* Play notification sound when a peer joins the call */
  const audio = new Audio(joinCallNotification)
  audio.load()
  audio.play()
}

resetChat = () =>{
  /* Empty the chat data */
  this.setState({messagesList: {data:[]}})
}

setLocalStreamAndVideoRef = (stream) => {
  /* bind the local stream to html video element */
  this.localVideoRef.current.srcObject = stream
  this.setState({ localStream: stream })
}

async setLocalConnection(){
  //intiate local connection
  const localConnection = await initiateConnection()
  //initiate data channels
  const SRChannel = await initiateDataChannel(localConnection, "SRChannel", 1)
  const chatChannel = await initiateDataChannel(localConnection, "chatChannel", 2)
  const mediaInputChannel = await initiateDataChannel(localConnection, "mediaInputChannel", 3)
  /* Listen and send the mic and cam status to the other peer using data channel */
  mediaInputChannel.addEventListener("open", () =>{
    this.mediaInputChannelSendMessage("mic " + this.state.mic)
      this.mediaInputChannelSendMessage("cam " + this.state.cam)
  })
  /* Update local connection and data channels state and initiate firebase instance */
  this.setState({
    database: firebase.database(),
    localConnection,
    SRChannel,
    chatChannel,
    mediaInputChannel
  },()=>{
    this.state.SRChannel.onmessage = (e) => {this.SRChannelRecieveMessage(e)}
    this.state.chatChannel.onmessage = (e) => {this.ChatChannelRecieveMessage(e)}
    this.state.mediaInputChannel.onmessage = (e) => {this.mediaInputChannelRecieveMessage(e)}
  })
}

appendOwnMessage = (msg) => {
  /* send chat message and update messageList state */
  if (this.state.chatChannel.readyState === "open"){
    const messagesList = this.state.messagesList
    if(msg){
      messagesList.data.push({type: 'sent', message: msg})
      this.setState({messagesList: messagesList})
    }
  }
  
}

/* SEND DATA */

/* media Input channel*/
mediaInputChannelSendMessage = (msg) => {
  if(msg && this.state.mediaInputChannel.readyState === 'open')
  this.state.mediaInputChannel.send(msg)
}

/* Chat Channel */
ChatChannelSendMessage = (msg) => {
  if(msg && this.state.chatChannel.readyState === 'open')
  this.state.chatChannel.send(msg)
}

/* SR channel */
SRChannelSendMessage = (msg) => {
  if(msg && this.state.SRChannel.readyState === 'open')
  this.state.SRChannel.send(msg)
}

/* Receive DATA */

/* Chat Channel */
ChatChannelRecieveMessage = (e) => {
  const messagesList = this.state.messagesList
  if(e.data)
  messagesList.data.push({type: 'received', message: e.data})
  this.setState({messagesList: messagesList},() => {this.playChatNotificationSound()})
  
}

/* SR channel */
SRChannelRecieveMessage = (e) => {
  this.setState({remoteTranslation: e.data})
}

/* media Input Chanel */
mediaInputChannelRecieveMessage = (e) => {
  /* Update other peer mic/cam status */
  const res = e.data.split(' ')
  switch(res[0]){
    case 'mic':
      res[1] === 'true'? this.setState({remoteMicStatus: true}):this.setState({remoteMicStatus: false})
      break
    case 'cam':
      res[1] === 'true'? this.setState({remoteCamStatus: true}):this.setState({remoteCamStatus: false})
      break
    default:
      break
  }
}

toggleChat = () =>{
  /* Toggle chat on and off */
  this.setState({chat: !this.state.chat})
}
toggleNoiseReduction = () => {
  /* Toggle noise reduction feature on and off */
  this.setState({NoiseReduction: !this.state.NoiseReduction})
}

toggleSubtitles = () => {
  /* Toggle subtitles on and off */
  this.setState({subtitles: !this.state.subtitles},()=>{console.log("sub: ",this.state.subtitles)})
  this.showCaptionInfo()
}

updateUsername = (username) => {
  /* Update username state on login  */
   this.setState({username: username},this.onLogin)
 }

 toggleMic = () =>{
   /* Toggle mic on and off */
   this.setState({mic: !this.state.mic},()=>{
     this.toggleInput()
     this.mediaInputChannelSendMessage("mic " + this.state.mic)
    })
}

 toggleCam = () =>{
   /* Toggle cam on and off */
  this.setState({cam: !this.state.cam},()=>{
    this.toggleInput()
    this.mediaInputChannelSendMessage("cam " + this.state.cam)
  })
  
}

toggleInput = () =>{
  /* Update local stream when user toggle on and off the mic/cam */
  if (this.state.localStream){
    const stream = this.state.localStream
    stream.getAudioTracks()[0].enabled = this.state.mic
    stream.getVideoTracks()[0].enabled = this.state.cam
    this.setState({
      localStream: stream
    })
   }
   else{
    navigator.mediaDevices
    .getUserMedia({video: true,audio: true})
    .then(stream =>{
      stream.getVideoTracks()[0].enabled = this.state.cam
      stream.getAudioTracks()[0].enabled = this.state.mic
      this.localVideoRef.current.srcObject = stream
      this.setState({
        localStream: stream
      })
      })
    .catch(console.log);
   }
  
}

setRemoteVideoRef = ref => {
  this.remoteVideoRef = ref
}

onLogin = async () => {
  /* When user log in execute doLogin function to update the database */
  return await doLogin(this.state.username, this.state.database, this.handleUpdate)
}

handleUpdate = (notif) => {
  /* This function handle the establishment of connection between two peers and update the UI accordingly */
  const { localConnection, database, localStream } = this.state
  if (notif) {
    switch (notif.type) {

      case 'offer':
        this.setState({
          connectedUser: notif.from
        })
        listenToConnectionEvents(localConnection, this.state.username, notif.from, database, this.remoteVideoRef, doCandidate)
        sendAnswer(localConnection, localStream, notif, doAnswer, database, this.state.username)
        this.setState({chat: true},()=>{
          this.playjoinCallNotificationSound()
          
        })
        break

      case 'answer':
        this.setState({
          connectedUser: notif.from
        })
        startCall(localConnection, notif)
        /* When call starts enbale chat and play notification sound */
        this.setState({chat: true},()=>{
        this.playjoinCallNotificationSound()
        })
        break

      case 'candidate':
        addCandidate(localConnection, notif)
        break

      case 'leave':
        this.setState({
        connectedUser: null
        },()=>{
          let senders = this.state.localConnection.getSenders()
          senders.forEach(sender =>{
            this.state.localConnection.removeTrack(sender)
          })
          this.onLogin()
          this.playEndCallNotificationSound()
          /* When call ends display the rating box to the user */
          this.setDisplayRating()
          this.setState({chat: false})
          this.resetChat()
        })
        
        break
      default:
        break
    }
  }
}

startCall = (userToCall) =>{
  /* This function intiate the call and updates the database */
  const { localConnection, database, localStream } = this.state
  listenToConnectionEvents(localConnection, this.state.username, userToCall, database, this.remoteVideoRef, doCandidate)
  // create an offer
  createOffer(localConnection, localStream, userToCall, doOffer, database, this.state.username)
}

leaveCall = () =>{
  /* This funtion handles the disconnection of one of the peers */
  if (this.state.connectedUser){
    doLeaveNotif(this.state.connectedUser,this.state.database,this.state.username)  
    doLeaveNotif(this.state.username,this.state.database,this.state.connectedUser)
  } 
}

updateTranslation = (transcript) => {
  /*Send the locally transcribed speech to the other peer */
  this.setState({localTranslation: transcript},this.sendMessage(this.state.localTranslation))
}

/*
handleChangeLanguage = (language) => {
  this.setState({language: language}) 
}
*/

shouldComponentUpdate (nextProps, nextState) {
  /* prevent application re-render when these states update */
  if (this.state.database !== nextState.database) {
    return false
  }
  if (this.state.localStream !== nextState.localStream) {
    return false
  }
  if (this.state.localConnection !== nextState.localConnection) {
    return false
  }
  if (this.state.dataChannel !== nextState.dataChannel) {
    return false
  }
  if (this.state.receiveChannel !== nextState.receiveChannel) {
    return false
  }
  if (this.state.username !== nextState.username) {
    return false
  }
  return true
}

handleInvite = () =>{
  if(this.state.connectedUser)
  return
  this.setState({invite: !this.state.invite})
}

handleShareScreen = () => {
  /* This function handle the screen sharing feature */
  if(this.state.connectedUser)
  {
  navigator.mediaDevices.getDisplayMedia({cursor: true}).then(stream =>{
    const screenTrack = stream.getVideoTracks()[0]
    let sender = this.state.localConnection.getSenders().find(s => {
      return s.track.kind === "video"
    })
    this.setState({sender: sender, screeShare: true})
    /* Remove old video track */
    this.state.localStream.removeTrack(this.state.localStream.getVideoTracks()[0])
    /* Add and replace the video stream with the screen feed */
    this.state.localStream.addTrack(screenTrack)
    this.state.sender.replaceTrack(screenTrack)
    this.state.localStream.getVideoTracks()[0].onended = (e) => {
    /* Handle the end of screen sharing */
     navigator.mediaDevices
    .getUserMedia({video: {deviceId: this.state.camId },
      audio: {deviceId: this.state.micId, sampleSize: 16}
})
    .then(stream =>{
      stream.getVideoTracks()[0].enabled = this.state.cam
      stream.getAudioTracks()[0].enabled = this.state.mic
      this.localVideoRef.current.srcObject = stream
      this.setState({
        localStream: stream
      },()=>{
        this.state.sender.replaceTrack(this.state.localStream.getVideoTracks()[0])
        this.setState({screeShare:false})
      })
      })
    .catch(console.log);
    }
  })
}
}

setDisplayRating = () =>{
  this.setState({displayRating: !this.state.displayRating})
}

showCaptionInfo = () =>{
  this.setState({captionInfo: true},()=>{
    setTimeout(()=>this.setState({captionInfo: false}),6000)
  })
}

handleTranscriptDownload = () => {
  /* This function handle the download of the transcribed audio after call ends */
  const element = document.createElement("a");
  const file = new Blob([this.state.remoteTranslation], {type: 'text/plain'});
  element.href = URL.createObjectURL(file);
  element.download = "Transcript.txt";
  document.body.appendChild(element); // Required for this to work in FireFox
  element.click();
}

clearTranscript = () => {
  /* Clear the transcription after call end */
  this.setState({
    localTranslation: '',
    remoteTranslation: ''
  })
}

updateMicId = (id) =>{
  this.setState({micId: id})
}

updateCamId = (id) => {
  this.setState({camId: id})
}

 render(){
  window.addEventListener('beforeunload', (event) => {
    // Cancel the event as stated by the standard.
    event.preventDefault();
    if (this.state.username){
      removeConnectedUser(this.state.database,this.state.username)
      if(this.state.connectedUser)
        this.leaveCall()
    }
    // Chrome requires returnValue to be set.
    event.returnValue = '';
  });

  function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  
  return(
    <div>
      
      {this.state.invite?<InviteBox
      startCall={this.startCall}
      handleInvite={this.handleInvite}
      database={this.state.database}
      username = {this.state.username}
      />:<></>}

      <DialogBox 
        mic={this.state.mic} 
        cam={this.state.cam}
        username={this.props.username} 
        database={this.state.database}
        toggleMic={this.toggleMic} 
        toggleCam={this.toggleCam}
        updateUsername={this.updateUsername} 
        onLogin={this.onLogin}
        setLocalStreamAndVideoRef={this.setLocalStreamAndVideoRef}
        updateMicId={this.updateMicId}
        updateCamId={this.updateCamId}
      />

      {this.state.chat?<Chat
      ChatChannelSendMessage={this.ChatChannelSendMessage}
      appendOwnMessage={this.appendOwnMessage}
      messagesList={this.state.messagesList}
      />:<></>}
    
      {this.state.displayRating?<RatingDiag
      database = {this.state.database}
      username = {this.state.username}
      setDisplayRating = {this.setDisplayRating}
      handleTranscriptDownload = {this.handleTranscriptDownload}
      clearTranscript = {this.clearTranscript}
      displayRating={this.state.displayRating}
      />:<></>}

       <div className="main">
      <FloatingActionMenu
          mic={this.state.mic}
          toggleSubtitles={this.toggleSubtitles}
          toggleMic={this.toggleMic}
          cam={this.state.cam} 
          toggleCam={this.toggleCam}
          leaveCall={this.leaveCall}
          handleInvite={this.handleInvite}
          handleShareScreen={this.handleShareScreen}
          toggleChat={this.toggleChat}
          chat={this.state.chat}
          subtitles={this.state.subtitles}  

      />
          <VideoContainer 
            username={this.state.username} 
            connectedUser={this.state.connectedUser}
            robolink={this.state.robolink} 
            localVideoRef={this.localVideoRef} 
            remoteVideoRef={this.remoteVideoRef} 
            remoterobolink={this.state.remoterobolink}
            setRemoteVideoRef={this.setRemoteVideoRef}
            sendMessage={this.SRChannelSendMessage}
            subtitles={this.state.subtitles}
            localTranslation={this.state.localTranslation}
            remoteTranslation={this.state.remoteTranslation}
            mic={this.state.mic}
            cam={this.state.cam}
            language={this.state.language}
            remoteCamStatus={this.state.remoteCamStatus}
            remoteMicStatus={this.state.remoteMicStatus}
          />
      </div>

      <img src={talanLogo} className="talanLogo" />
      {this.state.captionInfo?
      <Alert className='' id="test" severity="info">This is an experimental feature. Live caption 
      requires the other user to be using Chrome.</Alert>:<></>}
    </div>
    

  );
 }
    
  
  }

export default Main;
