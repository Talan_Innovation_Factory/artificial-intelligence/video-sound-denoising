<div align="center">

<img src="../images/talan_meet_logo.png" >


<br>💻📱👔💼
<br>
**A noise reduction & speech transcription video conferencing solution**

</div>



## Description 📝
This is a one-on-one video chat React application with several features such as text messaging, screen sharing, video and audio conferencing, real-time noise reduction and speech transcription.<br> 
A deep neural network model is used for noise reduction and Microsoft speech service is used for speech transcription.

To test the implementation, <a href="https://talan-meet.netlify.app/">visit the live app.</a>
## Setting up development environment ⚙️ 
- Fork or clone this repo.
- Install nodejs and NPM
- Visit Firebase console webpage, start a new project and copy Firebase configuration to the /src/config.js file

## How to build/run the project 🛠
- `npm install` to install all the dependencies
- `npm start ` to start a development server
  
## Screenshots 📸
- **Landing page**
<div align="center">
<img src="../images/landing_page.png" >
<br>
</div>

- **The main interface** 
<div align="center">
<img src="../images/main_interface.jpg" width=900 >
<br>
</div>

- **Joining a call**
<div align="center">
<img src="../images/joining_call.jpg" width=900>
<br>
</div>
