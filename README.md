<div align="center">

<img src="./images/talan_meet_logo.png" >


<h2 align="center">A noise reduction & speech transcription video conferencing solution </h2>

<p align="center">
<h3 align="center"><a href="https://talan-meet.netlify.app/">Visit the live app</a>|
<a href="https://talan_innovation_factory.gitlab.io/artificial-intelligence/noisereductiondemopage/">Try noise reduction</a></br>💻📱👔💼</h3>
</p>
<p align="center">
<img src="./images/technologies.png" height="200" width="550">
</p>
</div>

## Description 📝

The main purpose of this project is to implement a noise reduction model for removing background noise from video conferencing during a meeting and translate speech to text in real-time.
the solution takes as input the audio stream from the microphone, removing background noise locally before broadcasting it to the members of the meeting. Live captions of the speech are also displayed with the option of downloading them after the meeting ends.  


This repository contains the following:

- The training details of a Dual-signal Transformation LSTM Network used for the purpose of noise reduction <a href="./DTLN%20model">(Here)</a>.
- The front-end application (a **React** application) for video conferencing that uses the trained noise reduction model <a href="./video-chat-app">(Here)</a>.
 
## Features 💡

• Remove background noise in a video conference in real time.
• Convert any spoken word to written text in real time.
• Integrate these features into a video conference service.

## Architecture 👷‍♂️
The Figure below represents our global application architecture where we used **WebRTC** to establish a peer-to-peer connection between two users.  

A Firebase Realtime Database is used for the signaling process. Noise reduction models are deployed in the client side, this means that the enhancement of the audio of each user is done locally before it gets sent to the other peer.  

For the Realtime speech recognition, we used an existing API for many reasons that are explained in further sections of this document.   
<div align="center">
<img src="./images/architecture.png">
</div>

## How to use ? 👩‍🏫 
to use the Talan Meet you can follow these steps:
1. **Step 1:** Giving permissions and selecting the input media device
Accessing the microphone or the webcam from the browser require the user to explicitly grant permissions to our application.
In some cases the user might have multiples microphones or cameras attached to his device. Choosing which input device is used during a video-conference is a basic feature our application provide by requesting a list of the available media inputs.
<div align="center">
<img src="./images/step1.png" align="middle">
</div>

2. **Step2 :** Joining call and Enabling features
In this step, the user must entering a valid username then, selecting the input devices and choosing to enable or disable the media tracks.
List of available media input
<div align="center">
<img src="./images/step2.png" align="middle">
</div>
Once The ’JOIN NOW’ button is clicked, the user is introduced to the main interface which is intuitive to the average user with the main controls to the left and the local stream to the bottom-left.</br>

<div align="center">
<img src="./images/main_interface.jpg" width="900">
</div>

3. **Step 3:** Joining a call
Starting a call is a straightforward process, clicking the invite button prompt the user with a simple a dialog box to enter the username of the call partner. Once the username is valid and the other peer is connected, the connection is established between the two peers and the remote stream is displayed in the main interface like shown in the figure below :
<div align="center">
<img src="./images/joining_call.jpg" width="900">
</div>

**The different features can now be used after joining the call:** 

</br>

  - **Text chat:** Another functionality that our application offers is the ability to use text messaging in addition to voice and video.
<div align="center">
<img src="./images/text_messaging.PNG">
</div>

  - **Screen sharing:** </br>
Screen sharing is one of the main features present in the majority of the video-conferencing applications.
This feature is activated by the click on the screen sharing button.
<div align="center">
<img src="./images/screen_sharing.png" height="100" width="100">
</div>

  - **Activating speech transcription feature:** </br>
This feature is called also speech-to-text, It represents the ability to identify words spoken aloud and convert them into readable text.
In this application, captioning and subtitling option provides human transcribers/editors with the capability to review and simply tweak the output.
<div align="center">
<img src="./images/speech_transcription.png" height="100" width="100">
</div>
When it activated, the transcripted text appears in the main interface of the call.
<div align="center">
<img src="./images/live_text_transcription.jpg" width=900 >
</div>

  - **Rating and downloading speech transcript:** </br>
After a call is ended by the user, a dialog box appears of each user allowing him to first, download a text file containing the speech transcript of the whole conversation. Second, the user is able to submit a comment and rate the video/audio quality of the call.
<div align="center">
<img src="./images/post_call_dialogue.png" height="250" width="350">
</div>

## Demo 🚀
You can try the application and its features by following the next link for a live application:</br>
https://talan-meet.netlify.app/

## what's missing ?📌
There are some features missing which should exist in the final product:
- Noise reduction feature from real-time chat due to slow inference speed (working on fixing this currently).
- Real-time speech transcription ( Deactivated untilfixing a bug accuring when running multiple instances of the application simultaneously).
## Licence 🗣
[MIT](https://opensource.org/licenses/MIT)
